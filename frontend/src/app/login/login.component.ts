import { Loginmodel } from './../loginmodel';
import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material';
import { UsersService } from '../users.service';
import {Response} from '../response';
import { Router } from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLoading: boolean;
  snackbar: MatSnackBar;
  model: Loginmodel;
  userService: UsersService;
  constructor(snackbar: MatSnackBar, userService: UsersService, private router: Router) {
    this.isLoading = false;
    this.snackbar = snackbar;
    this.model = new Loginmodel();
    this.userService = userService;
   }

  Login() {
    this.isLoading = true;
    this.userService.Login(this.model).subscribe(
      (data: Response) => {
        this.isLoading = false;
        console.log(this.model);
        console.log(data);
        if (data.msg === 'success') {
          this.Error('Login Success!');
          this.router.navigate(['/Panel']);
        } else {
          this.Error('Username and Password combinations does not match any account!');
        }
      },
      error => {
        console.log(error);
        this.isLoading = false;
        this.Error(error);
      }
    );

  }

  Error(msg: string) {
    this.snackbar.open(msg, 'Dissmiss');
  }

  ngOnInit() {
  }

}
