import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Loginmodel } from './loginmodel';
import { Registermodel } from './registermodel';
import {CheckAuthRequest} from './check-auth-request';
import {LogoffRequest} from './logoff-request';
@Injectable({
  providedIn: 'root'
})
export class UsersService {
  private url = 'http://localhost:8000/api/user/';
  constructor(private http: HttpClient) { }
  Login(loginModel: Loginmodel) {
    console.log(loginModel);
    return this.http.post(this.url + 'login' , loginModel);
   }
  Register (registerModel: Registermodel ) {
    return this.http.post(this.url + 'register' , registerModel);
   }
  Logoff() {
    const model = new LogoffRequest();
    console.log(model);
    return this.http.post(this.url + 'log-off', model);
  }
  CheckAuth() {
    const model = new CheckAuthRequest();
    console.log(model);
    return this.http.post(this.url + 'check-auth', model, {withCredentials: true});
  }
}
