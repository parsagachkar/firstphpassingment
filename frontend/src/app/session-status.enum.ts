export enum SessionStatus {
    Checking, NotAuthorized, Authorized
}
