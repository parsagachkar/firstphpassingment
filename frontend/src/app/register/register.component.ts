import { Component, OnInit } from '@angular/core';
import { Registermodel } from '../registermodel';
import { UsersService } from '../users.service';
import {Response} from '../response';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  isLoading: boolean;
  model: Registermodel;
  repeatPassword: String;
  constructor(private usersService: UsersService, private snackbar: MatSnackBar, private router: Router) {
    this.isLoading = false;
    this.model = new Registermodel();
   }
  Register() {
    this.isLoading = true;
    console.log(this.model);
    const date: Date = new Date(Date.parse(this.model.dateofbirth));
    this.model.dateofbirth =
    date.getFullYear() + '/' + date.getMonth() + '/' + date.getDate();
    this.usersService.Register(this.model).subscribe(
      (data: Response) => {
        this.isLoading = false;
        console.log(data);
        if (data.msg === 'success') {
          this.Error('SingUp Success!');
          this.router.navigate(['/Login']);
        } else {
          this.Error('Provided Email is already associated with another account!');
        }
      },
      error => {
        this.isLoading = false;
        this.Error(error);
      }
    );
  }

  Error(msg: string) {
    this.snackbar.open(msg, 'Dissmiss');
  }
  ngOnInit() {
  }

}
