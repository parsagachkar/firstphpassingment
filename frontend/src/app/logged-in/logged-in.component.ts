import { Component, OnInit } from '@angular/core';
import { SessionStatus } from '../session-status.enum';
import { UsersService } from '../users.service';
import {Response} from '../response';
import {MatSnackBar} from '@angular/material';
import { Router } from '@angular/router';
@Component({
  selector: 'app-logged-in',
  templateUrl: './logged-in.component.html',
  styleUrls: ['./logged-in.component.css']
})
export class LoggedInComponent implements OnInit {
  SessionStatus = 0;
  constructor(private userService: UsersService, private snackbar: MatSnackBar, private router: Router) {  }
  checkStatus() {
    this.SessionStatus = 0;
    this.userService.CheckAuth().subscribe(
      (data: Response) => {
        console.log(data);
        if (data.msg === 'success') {
          this.SessionStatus = 2;
        } else {
          this.SessionStatus = 1;
        }
      },
      error => {
        this.SessionStatus = 1;
        this.snackbar.open('There was an error while checking session status from sever probebly you are disconnected!', 'Dismiss');
      }
      );
  }
  logOff() {
    this.userService.Logoff().subscribe(
      (data: Response) => {
        this.SessionStatus = 1;
        this.snackbar.open('Logoff Successful!', 'Dismiss');
        this.router.navigate(['/Login']);
      },
      error => {
        this.SessionStatus = 1;
        this.snackbar.open('There was an error while LogginOff from sever probebly you are disconnected!', 'Dismiss');
      }
      );
   }
  ngOnInit() {
    this.checkStatus();
  }

}
