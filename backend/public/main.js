(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<link rel=\"stylesheet\" href=\"/assets/FontAwesome/web-fonts-with-css/css/fontawesome-all.min.css\">\n<div>\n\n  <mat-toolbar color=\"primary\">\n    <span>\n      <i class=\"fa\"></i>\n    </span>\n    <span>First PHP Assingment</span>\n  </mat-toolbar>\n  <router-outlet></router-outlet>\n  \n</div>\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'frontend';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/input */ "./node_modules/@angular/material/esm5/input.es5.js");
/* harmony import */ var _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/progress-spinner */ "./node_modules/@angular/material/esm5/progress-spinner.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _register_register_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./register/register.component */ "./src/app/register/register.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _logged_in_logged_in_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./logged-in/logged-in.component */ "./src/app/logged-in/logged-in.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















var approuts = [
    { path: '', redirectTo: '/Login', pathMatch: 'full' },
    { path: 'Login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"] },
    { path: 'Register', component: _register_register_component__WEBPACK_IMPORTED_MODULE_9__["RegisterComponent"] },
    { path: 'Panel', component: _logged_in_logged_in_component__WEBPACK_IMPORTED_MODULE_12__["LoggedInComponent"] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_8__["LoginComponent"],
                _register_register_component__WEBPACK_IMPORTED_MODULE_9__["RegisterComponent"],
                _logged_in_logged_in_component__WEBPACK_IMPORTED_MODULE_12__["LoggedInComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_2__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatCardModule"],
                _angular_material_input__WEBPACK_IMPORTED_MODULE_5__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatButtonModule"],
                _angular_material_progress_spinner__WEBPACK_IMPORTED_MODULE_6__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatDatepickerModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_7__["RouterModule"].forRoot(approuts),
                _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatSnackBarModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["ReactiveFormsModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClientModule"]
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/check-auth-request.ts":
/*!***************************************!*\
  !*** ./src/app/check-auth-request.ts ***!
  \***************************************/
/*! exports provided: CheckAuthRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckAuthRequest", function() { return CheckAuthRequest; });
var CheckAuthRequest = /** @class */ (function () {
    function CheckAuthRequest() {
        this.Action = 'CheckAuth';
    }
    return CheckAuthRequest;
}());



/***/ }),

/***/ "./src/app/logged-in/logged-in.component.css":
/*!***************************************************!*\
  !*** ./src/app/logged-in/logged-in.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/logged-in/logged-in.component.html":
/*!****************************************************!*\
  !*** ./src/app/logged-in/logged-in.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form-div\">\n  <mat-card>\n    <div *ngIf=\"SessionStatus == 0\">\n      <h1 class=\"center\">\n        Checking Status\n        <div class=\"center\">\n          <i class=\"far fa-hourglass-end\"></i>\n        </div>\n      </h1>\n      \n    </div>\n    <div *ngIf=\"SessionStatus == 1\">\n        <h1 class=\"center\">\n          Not Authanticated!\n          <div class=\"center\"><i class=\"fal fa-times-circle\"></i><br></div>\n        </h1>\n        <div class=\"center\">\n          \n          <a mat-button routerLink=\"/Login\">Login</a><span> or </span>\n          <a mat-button routerLink=\"/Register\">SignUp</a>\n        </div>\n    </div>\n    <div *ngIf=\"SessionStatus == 2\">\n      <h1 class=\"center\">\n        Authanticated!\n        <div class=\"center\"><i class=\"fal fa-check-circle\"></i><br></div>\n      </h1>\n      <div class=\"center\">\n        <a mat-button (click)=\"logOff()\">Logoff</a>\n      </div>\n  </div>\n  </mat-card>\n</div>"

/***/ }),

/***/ "./src/app/logged-in/logged-in.component.ts":
/*!**************************************************!*\
  !*** ./src/app/logged-in/logged-in.component.ts ***!
  \**************************************************/
/*! exports provided: LoggedInComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoggedInComponent", function() { return LoggedInComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../users.service */ "./src/app/users.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoggedInComponent = /** @class */ (function () {
    function LoggedInComponent(userService, snackbar, router) {
        this.userService = userService;
        this.snackbar = snackbar;
        this.router = router;
        this.SessionStatus = 0;
    }
    LoggedInComponent.prototype.checkStatus = function () {
        var _this = this;
        this.SessionStatus = 0;
        this.userService.CheckAuth().subscribe(function (data) {
            console.log(data);
            if (data.msg === 'success') {
                _this.SessionStatus = 2;
            }
            else {
                _this.SessionStatus = 1;
            }
        }, function (error) {
            _this.SessionStatus = 1;
            _this.snackbar.open('There was an error while checking session status from sever probebly you are disconnected!', 'Dismiss');
        });
    };
    LoggedInComponent.prototype.logOff = function () {
        var _this = this;
        this.userService.Logoff().subscribe(function (data) {
            _this.SessionStatus = 1;
            _this.snackbar.open('Logoff Successful!', 'Dismiss');
            _this.router.navigate(['/Login']);
        }, function (error) {
            _this.SessionStatus = 1;
            _this.snackbar.open('There was an error while LogginOff from sever probebly you are disconnected!', 'Dismiss');
        });
    };
    LoggedInComponent.prototype.ngOnInit = function () {
        this.checkStatus();
    };
    LoggedInComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-logged-in',
            template: __webpack_require__(/*! ./logged-in.component.html */ "./src/app/logged-in/logged-in.component.html"),
            styles: [__webpack_require__(/*! ./logged-in.component.css */ "./src/app/logged-in/logged-in.component.css")]
        }),
        __metadata("design:paramtypes", [_users_service__WEBPACK_IMPORTED_MODULE_1__["UsersService"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], LoggedInComponent);
    return LoggedInComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form-div\">\n  <mat-card>\n    <h1 class=\"center\">\n      <i class=\"full-width\" ></i>\n      Login\n    </h1>\n    <form #loginForm=\"ngForm\">\n      <mat-form-field class=\"full-width\">\n        <input [(ngModel)]=\"model.email\" #email=\"ngModel\" pattern=\"^\\S+@\\S+[.]\\S+$\" name=\"email\" matInput type=\"email\" required placeholder=\"Email\"  >\n        <mat-error *ngIf=\"email.invalid\" >Email is not valid!</mat-error>\n      </mat-form-field>\n      <mat-form-field class=\"full-width\">\n        <input [(ngModel)]=\"model.passHash\" #password=\"ngModel\" name=\"password\" matInput type=\"password\" required placeholder=\"Password\"  >\n        <mat-error *ngIf=\"password.invalid\" >Password is required!</mat-error>\n      </mat-form-field>\n\n      <div *ngIf=\"!isLoading\">\n        <button mat-raised-button color=\"primary\" [disabled]=\"loginForm.invalid\" (click)=\"Login()\" class=\"full-width\">Login</button>\n        <a mat-button routerLink=\"/Register\" class=\"full-width space\">Register</a>\n      </div>\n    </form>\n    <div  *ngIf=\"isLoading\">\n      <mat-spinner class=\"center\"></mat-spinner>\n    </div>\n\n\n  </mat-card>\n</div>\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _loginmodel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../loginmodel */ "./src/app/loginmodel.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../users.service */ "./src/app/users.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var LoginComponent = /** @class */ (function () {
    function LoginComponent(snackbar, userService, router) {
        this.router = router;
        this.isLoading = false;
        this.snackbar = snackbar;
        this.model = new _loginmodel__WEBPACK_IMPORTED_MODULE_0__["Loginmodel"]();
        this.userService = userService;
    }
    LoginComponent.prototype.Login = function () {
        var _this = this;
        this.isLoading = true;
        this.userService.Login(this.model).subscribe(function (data) {
            _this.isLoading = false;
            console.log(_this.model);
            console.log(data);
            if (data.msg === 'success') {
                _this.Error('Login Success!');
                _this.router.navigate(['/Panel']);
            }
            else {
                _this.Error('Username and Password combinations does not match any account!');
            }
        }, function (error) {
            console.log(error);
            _this.isLoading = false;
            _this.Error(error);
        });
    };
    LoginComponent.prototype.Error = function (msg) {
        this.snackbar.open(msg, 'Dissmiss');
    };
    LoginComponent.prototype.ngOnInit = function () {
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"], _users_service__WEBPACK_IMPORTED_MODULE_3__["UsersService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/loginmodel.ts":
/*!*******************************!*\
  !*** ./src/app/loginmodel.ts ***!
  \*******************************/
/*! exports provided: Loginmodel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Loginmodel", function() { return Loginmodel; });
var Loginmodel = /** @class */ (function () {
    function Loginmodel() {
    }
    return Loginmodel;
}());



/***/ }),

/***/ "./src/app/logoff-request.ts":
/*!***********************************!*\
  !*** ./src/app/logoff-request.ts ***!
  \***********************************/
/*! exports provided: LogoffRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogoffRequest", function() { return LogoffRequest; });
var LogoffRequest = /** @class */ (function () {
    function LogoffRequest() {
    }
    return LogoffRequest;
}());



/***/ }),

/***/ "./src/app/register/register.component.css":
/*!*************************************************!*\
  !*** ./src/app/register/register.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/register/register.component.html":
/*!**************************************************!*\
  !*** ./src/app/register/register.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"form-div\" >\n  <mat-card >\n    <h1 class=\"center\">\n      <i class=\"full-width\" ></i>\n      Register\n    </h1>\n    <form #registerForm=\"ngForm\" (ngSubmit)=\"Register()\">\n      <mat-form-field class=\"full-width\">\n        <input  [(ngModel)]=\"model.email\" name=\"email\" #email=\"ngModel\" pattern=\"^\\S+@\\S+[.]\\S+$\" matInput type=\"email\"  required placeholder=\"Email\"  >\n        <mat-error *ngIf=\"email.invalid\" >Invalid Email</mat-error>\n      </mat-form-field>\n      <mat-form-field class=\"full-width\">\n\n        <input id=\"pass\" name=\"password\" [(ngModel)]=\"model.passhash\" #password=\"ngModel\" matInput type=\"password\" required placeholder=\"Password\"  >\n        \n        <mat-error *ngIf=\"password.invalid\" >Password is required</mat-error>\n      </mat-form-field>\n\n      <mat-form-field class=\"full-width\">\n        <input name=\"ssid\" [(ngModel)]=\"model.ssid\" pattern=\"^[0-9]{10}$\"  #ssid=\"ngModel\" matInput type=\"number\" required placeholder=\"SSID\"  >\n        <mat-error *ngIf=\"ssid.invalid\"  >SSID is Required and it should be 10 digits</mat-error>\n      </mat-form-field>\n      <mat-form-field class=\"full-width\">\n        <input name=\"firstname\" [(ngModel)]=\"model.firstname\" #firstname=\"ngModel\" matInput type=\"text\" required placeholder=\"First Name\"  >\n        <mat-error *ngIf=\"firstname.invalid\" >Firstname is required</mat-error>\n      </mat-form-field>\n      <mat-form-field class=\"full-width\">\n        <input name=\"lastname\" [(ngModel)]=\"model.lastname\" #lastname=\"ngModel\" matInput type=\"text\" required placeholder=\"Last Name\"  >\n        <mat-error *ngIf=\"lastname.invalid\" >Lastname is required</mat-error>\n      </mat-form-field>\n      <mat-form-field class=\"full-width\">\n        <input name=\"phonenumber\" [(ngModel)]=\"model.phonenumber\" #phonenumber=\"ngModel\" matInput type=\"tel\" required placeholder=\"Phone Number\"  >\n        <mat-error *ngIf=\"lastname.invalid\" >Phone Number is required</mat-error>\n      </mat-form-field>\n      <mat-form-field class=\"full-width\">\n        <input name=\"dateofbirth\" [(ngModel)]=\"model.dateofbirth\" #dateOfBirth=\"ngModel\" required matInput [matDatepicker]=\"picker\" placeholder=\"Date of Birth\">\n        <mat-datepicker-toggle matSuffix [for]=\"picker\"></mat-datepicker-toggle>\n        <mat-datepicker #picker></mat-datepicker>\n        <mat-error *ngIf=\"dateOfBirth.invalid\" >Date of Birth is required</mat-error>\n      </mat-form-field>\n\n      <div *ngIf=\"!isLoading\">\n        <button mat-raised-button color=\"primary\" [disabled]=\"registerForm.invalid\" type=\"submit\" class=\"full-width\">Register</button>\n        <a  mat-button routerLink=\"/Login\" class=\"full-width space\" >Already have an account?</a>\n      </div>\n      <div  *ngIf=\"isLoading\">\n        <mat-spinner class=\"center\"></mat-spinner>\n      </div>\n    </form>\n  </mat-card>\n</div>\n"

/***/ }),

/***/ "./src/app/register/register.component.ts":
/*!************************************************!*\
  !*** ./src/app/register/register.component.ts ***!
  \************************************************/
/*! exports provided: RegisterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegisterComponent", function() { return RegisterComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _registermodel__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../registermodel */ "./src/app/registermodel.ts");
/* harmony import */ var _users_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../users.service */ "./src/app/users.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var RegisterComponent = /** @class */ (function () {
    function RegisterComponent(usersService, snackbar, router) {
        this.usersService = usersService;
        this.snackbar = snackbar;
        this.router = router;
        this.isLoading = false;
        this.model = new _registermodel__WEBPACK_IMPORTED_MODULE_1__["Registermodel"]();
    }
    RegisterComponent.prototype.Register = function () {
        var _this = this;
        this.isLoading = true;
        console.log(this.model);
        var date = new Date(Date.parse(this.model.dateofbirth));
        this.model.dateofbirth =
            date.getFullYear() + '/' + date.getMonth() + '/' + date.getDate();
        this.usersService.Register(this.model).subscribe(function (data) {
            _this.isLoading = false;
            console.log(data);
            if (data.msg === 'success') {
                _this.Error('SingUp Success!');
                _this.router.navigate(['/Login']);
            }
            else {
                _this.Error('Provided Email is already associated with another account!');
            }
        }, function (error) {
            _this.isLoading = false;
            _this.Error(error);
        });
    };
    RegisterComponent.prototype.Error = function (msg) {
        this.snackbar.open(msg, 'Dissmiss');
    };
    RegisterComponent.prototype.ngOnInit = function () {
    };
    RegisterComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-register',
            template: __webpack_require__(/*! ./register.component.html */ "./src/app/register/register.component.html"),
            styles: [__webpack_require__(/*! ./register.component.css */ "./src/app/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [_users_service__WEBPACK_IMPORTED_MODULE_2__["UsersService"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], RegisterComponent);
    return RegisterComponent;
}());



/***/ }),

/***/ "./src/app/registermodel.ts":
/*!**********************************!*\
  !*** ./src/app/registermodel.ts ***!
  \**********************************/
/*! exports provided: Registermodel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Registermodel", function() { return Registermodel; });
var Registermodel = /** @class */ (function () {
    function Registermodel() {
    }
    return Registermodel;
}());



/***/ }),

/***/ "./src/app/users.service.ts":
/*!**********************************!*\
  !*** ./src/app/users.service.ts ***!
  \**********************************/
/*! exports provided: UsersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsersService", function() { return UsersService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _check_auth_request__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./check-auth-request */ "./src/app/check-auth-request.ts");
/* harmony import */ var _logoff_request__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./logoff-request */ "./src/app/logoff-request.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var UsersService = /** @class */ (function () {
    function UsersService(http) {
        this.http = http;
        this.url = 'http://localhost:8000/api/user/';
    }
    UsersService.prototype.Login = function (loginModel) {
        console.log(loginModel);
        return this.http.post(this.url + 'login', loginModel);
    };
    UsersService.prototype.Register = function (registerModel) {
        return this.http.post(this.url + 'register', registerModel);
    };
    UsersService.prototype.Logoff = function () {
        var model = new _logoff_request__WEBPACK_IMPORTED_MODULE_3__["LogoffRequest"]();
        console.log(model);
        return this.http.post(this.url + 'log-off', model);
    };
    UsersService.prototype.CheckAuth = function () {
        var model = new _check_auth_request__WEBPACK_IMPORTED_MODULE_2__["CheckAuthRequest"]();
        console.log(model);
        return this.http.post(this.url + 'check-auth', model, { withCredentials: true });
    };
    UsersService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], UsersService);
    return UsersService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! G:\Users\Parsa\Desktop\Inbox\PhpAssignment\frontend\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map