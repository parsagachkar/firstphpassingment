<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NotificationTest extends TestCase
{
    /**
     * Create test user
     *
     * Creates a user for testing
     *
     * @return void
     **/
    public function createTestUser()
    {
        $userService = new \App\Services\UserService(
            new \App\Repositories\UserRepository()
        );
        $email = "test@test.test";
        $name = "test";
        $LastName = "test";
        $password = "testpass";
        $PhoneNumber = "09214567896";
        $ssid = "0123456789";
        $date_of_birth  = '1993//10//14';
        $userService->CreateUser(
            $email, $name, $LastName, $password, $PhoneNumber, $ssid, $date_of_birth
        );
    }
    /**
     * Email Test.
     *
     * Sends a test Message to email!
     *
     * @return void
     * @test
     */
    public function emailTest()
    {
        $userRepo = new \App\Repositories\UserRepository();
        $emailService = new \App\Services\EmailService($userRepo);
        $this->createTestUser();
        $emailService->Send($userRepo->GetByEmail("test@test.test"));
        $this->assertTrue(true);
    }
    public function smsTest()
    {

        $this->assertTrue(true);
    }
}
