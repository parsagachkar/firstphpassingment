<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;


class ApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     * @test
     */
    public function registerTest()
    {
        $faker = \Faker\Factory::create("fa-IR");
        $user = [
            "email" => $faker->email,
            "passhash" => $faker->password,
            "dateofbirth" => $faker->dateTimeThisCentury->format('Y/m/d'),
            "firstname" => $faker->firstName,
            "lastname" => $faker->lastName,
            "ssid" => $faker->numberBetween(1000000000, 9999999999),
            "phonenumber" => $faker->phoneNumber,
        ];

        $resp = $this->json('POST', '/api/user/register', $user);
        $resp->assertStatus(200)->assertJson(['msg' => 'success']);

    }
    /**
     * Login Test
     *
     * Creates a test user then tries to login with it
     *
     * @return voice
     * @test
     **/
    public function loginTest()
    {
        $userService = new \App\Services\UserService(new \App\Repositories\UserRepository());
        $email = "test@test.test";
        $name = "test";
        $LastName = "test";
        $password = "testpass";
        $PhoneNumber = "09214567896";
        $ssid = "0123456789";
        $date_of_birth  = '1993//10//14';
        $userService->CreateUser( $email, $name, $LastName, $password, $PhoneNumber, $ssid, $date_of_birth );
        $creds =['email'=>$email,'passHash'=>$password];
        $resp = $this->json('POST', '/api/user/login', $creds);
        $resp->assertStatus(200)->assertJson(['msg' => 'success']);
    }

}
