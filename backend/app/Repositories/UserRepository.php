<?php
Namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\Hash;
class UserRepository
{
    function Get($info){
        return ($this->GetByEmail($info)?$this->GetByEmail($info):($this->GetByPhoneNumber($info)?$this->GetByPhoneNumber($info):($this->GetBySsid($info)?$this->GetBySsid($info):null)));
    }
    function GetByEmail($email){
        $user = (User::where('email', $email)->first()) or $user=null;
        return $user;
    }
    function GetByPhoneNumber($phonenumber){

        $user = (User::where('PhoneNumber', $phonenumber)->first()) or $user=null;
        return $user;
    }
    function GetBySsid($ssid){
        $user = (User::where('ssid', $ssid)->first()) or $user=null;
        return $user;
    }
    function NewUser($name, $email, $password, $ssid, $LastName, $PhoneNumber, $date_of_birth){
        if($this->Get($email) || $this->Get($ssid) || $this->Get($PhoneNumber)) return false;

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = sha1($password);
        $user->ssid = $ssid;
        $user->LastName = $LastName;
        $user->PhoneNumber = $PhoneNumber;
        $user->email_activation_code = rand(1000, 9999);
        $user->sms_activation_code = rand(1000, 9999);
        $user->save();
        return true;
    }

}
