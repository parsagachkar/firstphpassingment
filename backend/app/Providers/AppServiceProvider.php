<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        \App::bind("\App\Services\UserService",function(){
            return new \App\Services\UserService(new \App\Repositories\UserRepository());
        });
        \App::bind("\App\Services\EmailService",function(){
            return new \App\Services\EmailService(new \App\Repositories\UserRepository());
        });
        \App::bind("\App\Services\PhoneService",function(){
            return new \App\Services\PhoneService(new \App\Repositories\UserRepository());
        });
        //
    }
}
