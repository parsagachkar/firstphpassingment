<?php
Namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Hash;

class UserService{

    protected $users;

    function __construct($users){
        $this->users = $users;
    }


    function CreateUser( $email, $name, $LastName, $password, $PhoneNumber, $ssid, $date_of_birth ){
        $resp = $this->users->NewUser(
            $name, $email, $password, $ssid, $LastName, $PhoneNumber, $date_of_birth
        );


        return $resp;
    }

    function CheckUser($info,$password){
        $user = $this->users->Get($info);

        if($user && $user->password==sha1($password)){
            return true;
        }
        return false;
    }
    function Get($info){
        $user = $this->users->Get($info);
        return $user?$user:null;
    }


}
