<?php
Namespace App\Services;

use \App\Repositories\UserRepository;
class PhoneService
{
    protected $users;
    function __construct(UserRepository $users){
        $this->users = $users;
    }
    function Send($user)
    {
        $text = "Your activation Code is $user->sms_activation_code";
        $log = "Sending Sms Message to $user->PhoneNumber with Context '$text'";
        \Log::info($log);

    }
    function Verify($phone,$code)
    {
        $user = $this->users->GetByPhoneNumber($phone);
        if($user && $user->sms_activation_code == $code){
            $user->PhoneNumber_Verified_at = date("Y/m/d H:i:s");
            $user->save();
            return true;
        }
        return false;
    }
}

