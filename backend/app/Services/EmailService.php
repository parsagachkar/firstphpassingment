<?php
Namespace App\Services;
use \App\Repositories\UserRepository;
use Illuminate\Support\Facades\Mail;
class EmailService
{
    protected $users;
    function __construct(UserRepository $users){
        $this->users = $users;
    }
    function Send($user)
    {
        Mail::to($user->email)->send(
            new \App\Mail\ConfirmEmail($user->email_activation_code)
        );
    }
    function Verify($email,$code)
    {
        $user = $this->users->GetByEmail($email);
        if($user && $user->email_activation_code == $code){
            $user->email_verified_at = date("Y/m/d H:i:s");
            $user->save();
            return true;
        }
        return false;
    }
}


