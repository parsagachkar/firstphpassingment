<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Response;

use App\User;
use App\Repositories\UserRepository;
use \App\Services\UserService;
class UserController extends Controller
{
    protected $users;
    function __construct(){
        $this->users =  \App::make("\App\Services\UserService");
    }
    function Login(Request $request){

        $email = $request->input('email');
        $passHash = $request->input('passHash');

        if($this->users->CheckUser($email,$passHash)){
            $request->session()->put('user',sha1($email));
            return response()->json(["msg"=>"success"]);
        }

        return response()->json(["msg"=>"fail"]);
    }
    function Logoff(Request $request){
        $request->session()->flush();
        return response()->json(["msg"=>"success"]);
    }
    function CheckAuth(Request $request){
        return $request->session()->has('user')?response()->json(["msg"=>"success"]):response()->json(["msg"=>"fail"]);
    }
    function Register(Request $req){

        $email = $req->input('email');
        $name = $req->input('firstname');
        $LastName = $req->input('lastname');
        $password = $req->input('passhash');
        $PhoneNumber = $req->input('phonenumber');
        $ssid = $req->input('ssid');
        $date_of_birth = "";
        $resp = $this->users->CreateUser( $email, $name, $LastName, $password, $PhoneNumber, $ssid, $date_of_birth ) ? response()->json(["msg"=>"success"]): response()->json(["msg"=>"fail"]);
        $this->dispatch(new \App\Jobs\SendNotifications($email));
        return $resp;

    }
    function VerifyEmail(Request $req){

        $email = $req->input('email');
        $code = $req->input('email_activation_code');
        $emailsvc = \App::make("\App\Services\EmailService");

        return $emailsvc->Verify($email,$code) ?  response()->json(["msg"=>"success"]) : response()->json(["msg"=>"fail"]);
    }

    function VerifyPhone(Request $req){
        $phonenumber = $req->input('phonenumber');
        $code = $req->input('code');
        $phonesvc = \App::make("\App\Services\PhoneService");

        return $phonesvc->Verify($phonenumber,$code) ?  response()->json(["msg"=>"success"]) : response()->json(["msg"=>"fail"]);
    }
}
