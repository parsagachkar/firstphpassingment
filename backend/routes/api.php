<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/user/register', 'UserController@Register');
Route::post('/user/verify-email', 'UserController@VerifyEmail');
Route::post('/user/verify-phone', 'UserController@VerifyPhone');
Route::post('/user/login', 'UserController@Login');
Route::post('/user/check-auth', 'UserController@CheckAuth');
Route::post('/user/log-off', 'UserController@Logoff');


